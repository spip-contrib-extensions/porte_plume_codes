<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pp_codes?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pp_codes_description' => 'Gestione dei pulsanti per l’inserimento del codice tramite la barra di modifica di Portapenne.',
	'pp_codes_nom' => 'Codici per Portapenne',
	'pp_codes_slogan' => 'Aggiungi pulsanti alla barra degli strumenti di Portapenne per gestire i codici'
);
