<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pp_codes?lang_cible=mg
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pp_codes_description' => 'Gestion de boutons pour insérer du code informatique via la barre d’édition Porte Plume.',
	'pp_codes_nom' => 'Codes Informatiques pour Porte Plume',
	'pp_codes_slogan' => 'Ajoute des boutons à la barre d’outils du Porte Plume pour gérer des codes informatiques'
);
