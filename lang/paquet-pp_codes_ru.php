<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pp_codes?lang_cible=ru
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pp_codes_description' => 'Управление кнопками для вставки компьютерного кода с панели инструментов редактирования текста.
Добавляет ярлык <code>[->ecrire/inc_versions.php#trac]</code>.', # MODIF
	'pp_codes_nom' => 'Компьютерные коды',
	'pp_codes_slogan' => 'Добавляет кнопки на панель инструментов редактирования текста для вставки компьютерных кодов'
);
