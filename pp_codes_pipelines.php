<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

function pp_codes_porte_plume_barre_pre_charger($barres) {
	// on ajoute les boutons dans les 2 barres de SPIP
	foreach (['edition','forum'] as $nom) {
		$barre = &$barres[$nom];

		$barre->ajouterPlusieursApres('cadre', [
			// bouton <cadre spip>
			[
				'id'          => 'cadre_spip',
				'name'        => _T('pp_codes:outil_inserer_cadre_spip'),
				'className'   => 'outil_cadre_spip',
				'openWith' => "<cadre class='spip'>\n",
				'closeWith' => "\n</cadre>",
				'display'     => false,
			],
			// bouton <cadre php>
			[
				'id'          => 'cadre_php',
				'name'        => _T('pp_codes:outil_inserer_cadre_php'),
				'className'   => 'outil_cadre_php',
				'openWith' => "<cadre class='php'>\n",
				'closeWith' => "\n</cadre>",
				'display'     => false,
			],
			// bouton <cadre html>
			[
				'id'			=> 'cadre_html',
				'name'			=> _T('pp_codes:outil_inserer_cadre_html'),
				'className'		=> 'outil_cadre_html',
				'openWith'		=> "<cadre class='html4strict'>\n",
				'closeWith'		=> "\n</cadre>",
				'display'		=> false,
			],
			// bouton <cadre css>
			[
				'id'          => 'cadre_css',
				'name'        => _T('pp_codes:outil_inserer_cadre_css'),
				'className'   => 'outil_cadre_css',
				'openWith' => "<cadre class='css'>\n",
				'closeWith' => "\n</cadre>",
				'display'     => false,
			],
			// bouton <cadre xml>
			 [
				'id'          => 'cadre_xml',
				'name'        => _T('pp_codes:outil_inserer_cadre_xml'),
				'className'   => 'outil_cadre_xml',
				'openWith' => "<cadre class='xml'>\n",
				'closeWith' => "\n</cadre>",
				'display'     => false,
			],
			// bouton <cadre latex>
			 [
				'id'          => 'cadre_latex',
				'name'        => _T('pp_codes:outil_inserer_cadre_latex'),
				'className'   => 'outil_cadre_latex',
				'openWith' => "<cadre class='latex'>\n",
				'closeWith' => "\n</cadre>",
				'display'     => false,
			],
			// bouton <cadre bibtex>
			 [
				'id'          => 'cadre_bibtex',
				'name'        => _T('pp_codes:outil_inserer_cadre_bibtex'),
				'className'   => 'outil_cadre_bibtex',
				'openWith' => "<cadre class='bibtex'>\n",
				'closeWith' => "\n</cadre>",
				'display'     => false,
			],
			// bouton <cadre biblatex>
			 [
				'id'          => 'cadre_biblatex',
				'name'        => _T('pp_codes:outil_inserer_cadre_biblatex'),
				'className'   => 'outil_cadre_biblatex',
				'openWith' => "<cadre class='biblatex'>\n",
				'closeWith' => "\n</cadre>",
				'display'     => false,
			],
			// bouton <pre>
			[
				'id'          => 'pre',
				'name'        => _T('pp_codes:outil_inserer_pre'),
				'className'   => 'outil_pre',
				'openWith' => '<pre>',
				'closeWith' => '</pre>',
				'display'     => false,
			],
			// bouton <var>
			[
				'id'          => 'var',
				'name'        => _T('pp_codes:outil_inserer_var'),
				'className'   => 'outil_var',
				'openWith' => '<var>',
				'closeWith' => '</var>',
				'display'     => false,
			],
			// bouton <samp>
			[
				'id'          => 'samp',
				'name'        => _T('pp_codes:outil_inserer_samp'),
				'className'   => 'outil_samp',
				'openWith' => '<samp>',
				'closeWith' => '</samp>',
				'display'     => false,
			],
			// bouton <kbd>
			[
				'id'          => 'kbd',
				'name'        => _T('pp_codes:outil_inserer_kbd'),
				'className'   => 'outil_kbd',
				'openWith' => '<kbd>',
				'closeWith' => '</kbd>',
				'display'     => false,
			],
			// Lien vers Trac
			// trop specifique a SPIP pour etre affiche par defaut...
			/*
			array(
				"id"          => 'lienTrac',
				"name"        => _T('pp_codes:outil_inserer_lien_trac'),
				"className"   => 'outil_lien_trac',
				"openWith" => "[?",
				"closeWith" => "#trac]",
				"display"     => false,
			),
			*/
		]);
	}
	return $barres;
}


function pp_codes_porte_plume_barre_charger($barres) {
	// en fonction de la config, afficher ou non sur barre d'edition et forum
	// par defaut : edition = oui, forum = non
	// ce que donne deja pre_charger par ailleurs
	$pp = isset($GLOBALS['meta']['porte_plume']) ? @unserialize($GLOBALS['meta']['porte_plume']) : '';

	if (isset($pp['codes']) and $codes = $pp['codes']) {
		$activer = [];

		if ($codes['activer_barre_edition'] == 'on') {
			$activer[] = 'edition';
		}
		if ($codes['activer_barre_forum'] == 'on') {
			$activer[] = 'forum';
		}
		foreach ($activer as $nom) {
			if (isset($barres[$nom])) {
				$barre = &$barres[$nom];

				$outils_actifs = (isset($codes['outils_actifs']) and is_array($codes['outils_actifs'])) ? $codes['outils_actifs'] : [];
				if ($outils_actifs) {
					$barre->afficher($outils_actifs);
					$barre->afficher(['sepCode', 'grpCode']);
				}
			}
		}
	}
	return $barres;
}


function pp_codes_porte_plume_lien_classe_vers_icone($flux) {

	// Récupérer la définition des icônes vanillas afin d'en réutiliser certaines
	$definition_icones = charger_fonction('icones', 'barre_outils/edition');
	$icones = $definition_icones();

	return array_merge($flux, [
		'outil_cadre_spip' => 'cadre_spip.svg',
		'outil_cadre_php' => 'cadre_php.svg',
		'outil_cadre_html' => 'cadre_html.svg',
		'outil_cadre_xml' => 'cadre_xml.svg',
		'outil_cadre_latex' => 'cadre_latex.svg',
		'outil_cadre_biblatex' => 'cadre_biblatex.svg',
		'outil_cadre_bibtex' => 'cadre_bibtex.svg',
		'outil_cadre_css' => 'cadre_css.svg',
		'outil_pre' => 'pre_code.svg',
		'outil_samp' => 'samp_code.svg',
		'outil_var' => 'var_code.svg',
		'outil_kbd' => 'kbd_code.svg',
		//'outil_lien_trac'=>'trac_logo16.png',
	]);
}
